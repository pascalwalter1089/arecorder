package com.pascalwalter.arecorder.service;

import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.pascalwalter.arecorder.util.TimestampToStringConverter;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Timer;
import java.util.TimerTask;

public class TimeCodeService
{
    private final TextView mTimeCodeTextView;
    private final AppCompatActivity context;
    private int mDurationInSeconds;
    private static Timer mTimer;

    public TimeCodeService(TextView timeCodeTextView, AppCompatActivity ctx)
    {
        this.mTimeCodeTextView = timeCodeTextView;
        this.context = ctx;
    }

    private String getRecordingTimeAsString()
    {
        return TimestampToStringConverter.getFormattedTimestamp(this.mDurationInSeconds);
    }
    public void start()
    {
        mTimer = new Timer(true);
        TimerTask tt = new TimerTask()
        {
            @Override
            public void run()
            {
                TimeCodeService.this.mDurationInSeconds++;
                TimeCodeService.this.context.runOnUiThread(() -> {
                    TimeCodeService.this.mTimeCodeTextView.setText(TimeCodeService.this.getRecordingTimeAsString());
                });
            }
        };

        mTimer.scheduleAtFixedRate(tt, 1000, 1000);
    }

    public int getCurrentTimestamp()
    {
        return mDurationInSeconds;
    }

    public void stop()
    {
        mTimer.cancel();
        mTimer = null;
    }

    public void reset()
    {
        mDurationInSeconds = 0;
    }

    public void resume()
    {

    }
}
