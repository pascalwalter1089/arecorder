package com.pascalwalter.arecorder.service;

import com.pascalwalter.arecorder.StringConstants;
import com.pascalwalter.arecorder.models.Bookmark;
import com.pascalwalter.arecorder.models.Session;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class LoadSessionService
{
    public Session loadSession(String mSessionDirectoryName)
    {
        String sessionFileName = mSessionDirectoryName + StringConstants.XML_FILE_NAME;

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try
        {
            //dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            DocumentBuilder db = dbf.newDocumentBuilder();

            Document doc = db.parse(new File(sessionFileName));

            // optional, but recommended
            // http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            doc.getDocumentElement().normalize();
            NodeList list = doc.getElementsByTagName(StringConstants.KEY_SESSION_NAME);
            String sessionName = list.item(0).getTextContent();

            long date =
                    Long.parseLong(doc.getElementsByTagName(StringConstants.KEY_DATE).item(0).getTextContent());

            String file =
                    doc.getElementsByTagName(StringConstants.KEY_FILE).item(0).getTextContent();

            List<Bookmark> bookmarks = new ArrayList<>();

            NodeList bookmarkList = doc.getElementsByTagName(StringConstants.KEY_BOOKMARK);
            for (int i = 0; i < bookmarkList.getLength(); i++) {
                Node node = bookmarkList.item(i);
                String name = "";
                int secondsFromStart = 0;
                    if (node.getNodeType() == Node.ELEMENT_NODE)
                    {
                        Element element = (Element) node;
                        name =
                                element.getElementsByTagName(StringConstants.KEY_NAME).item(0).getTextContent();
                        secondsFromStart =
                                Integer.parseInt(element.getElementsByTagName(StringConstants.KEY_SECONDS_FROM_START).item(0).getTextContent());
                        bookmarks.add(new Bookmark(name, secondsFromStart));
                    }
            }


            return new Session(sessionName, bookmarks, new Date(date), sessionFileName);
        } catch (ParserConfigurationException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        } catch (SAXException e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
