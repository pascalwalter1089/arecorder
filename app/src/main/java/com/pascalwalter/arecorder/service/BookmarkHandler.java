package com.pascalwalter.arecorder.service;

import com.pascalwalter.arecorder.models.Bookmark;
import com.pascalwalter.arecorder.models.Session;

public class BookmarkHandler
{
    public Session addBookmark(Session session, int timestampFromStart, String name)
    {
        session.addBookmark(
                new Bookmark(
                        name,
                        timestampFromStart
                )
        );

        return session;
    }
}
