package com.pascalwalter.arecorder.service;

import com.pascalwalter.arecorder.StringConstants;
import com.pascalwalter.arecorder.models.Session;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class SaveSessionService
{
    public SaveSessionService() {

    }

    public boolean saveSession(Session session, String mSessionDirectoryName)
    {
        String xmlFileName = mSessionDirectoryName + StringConstants.XML_FILE_NAME;

        try
        {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement(StringConstants.KEY_SESSION);
            doc.appendChild(rootElement);

            Element sessionName = doc.createElement(StringConstants.KEY_SESSION_NAME);
            sessionName.setTextContent(session.getName());
            rootElement.appendChild(sessionName);

            Element sessionDate = doc.createElement(StringConstants.KEY_DATE);
            sessionDate.setTextContent(""+session.getDate().getTime());
            rootElement.appendChild(sessionDate);

            Element sessionFile = doc.createElement(StringConstants.KEY_FILE);
            sessionFile.setTextContent(session.getAudioFilePath());
            rootElement.appendChild(sessionFile);

            session.getBookmarks().forEach(b -> {
                Element sessionBookmark = doc.createElement(StringConstants.KEY_BOOKMARK);
                Element bookmarkMilliSecondsFromStart = doc.createElement(StringConstants.KEY_SECONDS_FROM_START);
                bookmarkMilliSecondsFromStart.setTextContent("" + b.getSecondsFromStart());
                Element bookmarkName = doc.createElement(StringConstants.KEY_NAME);
                bookmarkName.setTextContent(b.getName());

                sessionBookmark.appendChild(bookmarkName);
                sessionBookmark.appendChild(bookmarkMilliSecondsFromStart);

                rootElement.appendChild(sessionBookmark);
            });

            FileOutputStream output = new FileOutputStream(xmlFileName);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(output);

            transformer.transform(source, result);
        } catch (ParserConfigurationException e)
        {
            e.printStackTrace();
        } catch (TransformerConfigurationException e)
        {
            e.printStackTrace();
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        } catch (TransformerException e)
        {
            e.printStackTrace();
        }
        return true;
    }
}
