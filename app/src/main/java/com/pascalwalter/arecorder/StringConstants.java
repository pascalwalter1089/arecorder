package com.pascalwalter.arecorder;

public class StringConstants
{
    public static final String NEW_SESSION = "NEW_SESSION";
    public static final String LOAD_SESSION = "LOAD_SESSION";
    public static final String XML_FILE_NAME = "/session_data.xml";
    public static final String KEY_SESSION_NAME = "SessionName";
    public static final String KEY_DATE = "Date";
    public static final String KEY_FILE = "File";
    public static final String KEY_BOOKMARK = "Bookmark";
    public static final String KEY_SECONDS_FROM_START = "SecondsFromStart";
    public static final String KEY_NAME = "Name";
    public static final String KEY_SESSION = "Session";
}
