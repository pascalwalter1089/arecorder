package com.pascalwalter.arecorder;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.pascalwalter.arecorder.fragments.StartNewSessionDialogFragment;
import com.pascalwalter.arecorder.models.Session;
import com.pascalwalter.arecorder.service.LoadSessionService;

public class MainActivity extends AppCompatActivity implements StartNewSessionDialogFragment.NoticeDialogListener
{

    private Button mNewSessionButton;
    private Button mLoadSessionButton;
    private LoadSessionService mLoadSessionService;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNewSessionButton = findViewById(R.id.newSessionButton);
        mNewSessionButton.setOnClickListener(l -> startNewSession());

        // TODO: Make it possible to load an existing session (when exist)
        mLoadSessionButton = findViewById(R.id.loadSessionButton);
        mLoadSessionButton.setOnClickListener(l -> loadSession());
    }

    private void loadSession()
    {
        Intent intent = new Intent(MainActivity.this, RecordingActivity.class);
        intent.putExtra(StringConstants.LOAD_SESSION, "Bbb");
        startActivity(intent);
    }

    private void startNewSession()
    {
        DialogFragment dialog = new StartNewSessionDialogFragment();
        dialog.show(getSupportFragmentManager(), "NoticeDialogFragment");
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String sessionName)
    {
        dialog.dismiss();
        Intent intent = new Intent(MainActivity.this, RecordingActivity.class);
        intent.putExtra(StringConstants.NEW_SESSION, sessionName);
        startActivity(intent);
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog)
    {
        dialog.dismiss();
    }
}
