package com.pascalwalter.arecorder;

import static androidx.core.content.PackageManagerCompat.LOG_TAG;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pascalwalter.arecorder.adapter.BookmarkRecyclerViewAdapter;
import com.pascalwalter.arecorder.models.Bookmark;
import com.pascalwalter.arecorder.models.Session;
import com.pascalwalter.arecorder.service.AudioRecorder;
import com.pascalwalter.arecorder.service.BookmarkHandler;
import com.pascalwalter.arecorder.service.LoadSessionService;
import com.pascalwalter.arecorder.service.SaveSessionService;
import com.pascalwalter.arecorder.service.TimeCodeService;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class RecordingActivity extends AppCompatActivity
{
    private BookmarkHandler mBookmarkHandler;
    private TextView mTimeCodeTextView;
    private Button mRecordButton;
    private Button mAddBookmarkButton;
    private MediaRecorder mMediaRecorder;
    private boolean mIsRecording = false;
    private String fileName;
    private MediaPlayer player;
    private boolean mPlaying = false;
    private TimeCodeService mTimeCodeService;
    private Session mSession;
    private SaveSessionService mSaveSessionService;
    private RecyclerView mBookmarkList;
    private BookmarkRecyclerViewAdapter mBookmarkListRecyclerViewAdapter;
    private Button mPlayButton;
    private String mSessionDirectoryName;
    private LoadSessionService mLoadSessionService;

    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO: Make it possible to jump to position when bookmark is clicked
        // TODO: Make it possible to save a project (with xml!) https://developer.android.com/develop/ui/views/components/menus
        // TODO: Make it possible to load existing project https://developer.android.com/develop/ui/views/components/menus

        super.onCreate(savedInstanceState);
        setContentView(R.layout.recording_activity);

        Intent intent = getIntent();
        String sessionName = intent.getStringExtra(StringConstants.NEW_SESSION);
        String exisitingSession = intent.getStringExtra(StringConstants.LOAD_SESSION);
        if (sessionName != null) {
            mSessionDirectoryName = getExternalFilesDir(null).getAbsolutePath() + "/" + sessionName;
            File f = new File(mSessionDirectoryName);
            if (!f.isDirectory()) {
                f.mkdir();
            }
            fileName = mSessionDirectoryName + "/audio.m4a";

            mSession = new Session(
                    sessionName,
                    new ArrayList<>(),
                    new Date(),
                    fileName
            );
        } else if (exisitingSession != null) {

            mSessionDirectoryName = getExternalFilesDir(null).getAbsolutePath() + "/" + exisitingSession;
            File f = new File(mSessionDirectoryName);
            if (!f.isDirectory()) {
                f.mkdir();
            }
            fileName = mSessionDirectoryName + "/audio.m4a";

            mLoadSessionService = new LoadSessionService();
            mSession = mLoadSessionService.loadSession(mSessionDirectoryName);
        }

        mSaveSessionService = new SaveSessionService();
        mPlayButton = findViewById(R.id.playButton);
        mTimeCodeTextView = findViewById(R.id.timeCodeTextLabel);
        mRecordButton = findViewById(R.id.recordButton);
        mAddBookmarkButton = findViewById(R.id.addBookmarkButton);
        mBookmarkHandler = new BookmarkHandler();
        mBookmarkList = findViewById(R.id.bookmarkRecyclerView);
        mBookmarkListRecyclerViewAdapter =
                new BookmarkRecyclerViewAdapter(mSession.getBookmarks().toArray(new Bookmark[0]));
        mBookmarkList.setAdapter(mBookmarkListRecyclerViewAdapter);
        mBookmarkList.setLayoutManager(new LinearLayoutManager(this.getApplicationContext()));

        if (ContextCompat.checkSelfPermission(
                this.getApplicationContext(),
                Manifest.permission.RECORD_AUDIO
        ) != PackageManager.PERMISSION_GRANTED)
        {
            this.requestPermissions(
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    100
            );
        }


        mRecordButton.setOnClickListener(l -> {
            if (RecordingActivity.this.mIsRecording) {
                RecordingActivity.this.stopRecording();
            } else {
                RecordingActivity.this.startRecording();
            }
        });

        mPlayButton.setOnClickListener(l -> {
            if (!mPlaying && !mIsRecording) {
                startPlaying();
            } else if (mPlaying && !mIsRecording) {
                stopPlaying();
            }
        });

        mAddBookmarkButton.setOnClickListener(l -> {
            RecordingActivity.this.addBookmark();
        });

        mTimeCodeService = new TimeCodeService(mTimeCodeTextView, this);
    }

    private void addBookmark()
    {
        int timestamp = mTimeCodeService.getCurrentTimestamp();
        mSession = mBookmarkHandler.addBookmark(mSession, timestamp, "" + timestamp);
        mBookmarkListRecyclerViewAdapter.updateData(mSession.getBookmarks().toArray(new Bookmark[0]));
    }

    private void stopRecording()
    {
        mMediaRecorder.stop();
        mMediaRecorder.release();
        mMediaRecorder = null;
        mRecordButton.setText("O");
        mIsRecording = false;
        mTimeCodeService.stop();
    }

    @SuppressLint("RestrictedApi")
    private void startRecording()
    {
        //https://stackoverflow.com/questions/56854199/how-to-record-good-quality-audio-using-mediarecoder-in-android
        mMediaRecorder = new MediaRecorder();
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        mMediaRecorder.setAudioEncodingBitRate(16*44100);
        mMediaRecorder.setAudioSamplingRate(44100);

        mMediaRecorder.setOutputFile(fileName);
        try
        {
            mMediaRecorder.prepare();
        } catch (IOException e)
        {
            Log.e(LOG_TAG, "prepare() failed");
            e.printStackTrace();
        }
        mRecordButton.setText("||");
        mIsRecording = true;
        mMediaRecorder.start();
        mTimeCodeService.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mMediaRecorder != null) {
            mMediaRecorder.release();
            mMediaRecorder = null;
            mTimeCodeService.stop();
        }

        this.saveSession();
    }

    private void saveSession()
    {
        this.mSaveSessionService.saveSession(mSession, mSessionDirectoryName);
    }


    @SuppressLint("RestrictedApi")
    private void startPlaying() {
        player = new MediaPlayer();
        try {
            player.setDataSource(fileName);
            player.prepare();
            mPlaying = true;
            player.start();
            mPlayButton.setText("Stop");
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
    }

    private void stopPlaying() {
        player.release();
        player = null;
        mPlaying = false;
        mPlayButton.setText("Play");
    }
}
