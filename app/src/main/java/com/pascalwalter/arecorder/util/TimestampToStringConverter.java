package com.pascalwalter.arecorder.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class TimestampToStringConverter
{
    public static String getFormattedTimestamp(int timestamp)
    {
        int hours = timestamp / 3600;
        int minutes = timestamp / 60 % 60;
        int seconds = timestamp % 60;

        NumberFormat f = new DecimalFormat("00");
        return String.format(
                "%s:%s:%s",
                f.format(hours),
                f.format(minutes),
                f.format(seconds)
        );
    }
}
