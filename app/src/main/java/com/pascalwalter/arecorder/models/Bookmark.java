package com.pascalwalter.arecorder.models;

public class Bookmark
{
    private String name;
    private int secondsFromStart;

    public Bookmark(String name, int secondsFromStart) {
        this.name = name;
        this.secondsFromStart = secondsFromStart;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getSecondsFromStart()
    {
        return secondsFromStart;
    }

    public void setSecondsFromStart(int secondsFromStart)
    {
        this.secondsFromStart = secondsFromStart;
    }
}
