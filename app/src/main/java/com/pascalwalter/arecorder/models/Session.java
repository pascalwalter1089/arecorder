package com.pascalwalter.arecorder.models;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class Session
{


    private String audioFilePath;
    private String name;
    private Date date;
    private List<Bookmark> bookmarks;

    public Session(String name, List<Bookmark> bookmarks, Date date, String audioFilePath)
    {
        this.name = name;
        this.date = date;
        this.bookmarks = bookmarks;
        this.audioFilePath = audioFilePath;
    }
    public String getAudioFilePath()
    {
        return audioFilePath;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public List<Bookmark> getBookmarks()
    {
        return bookmarks;
    }

    public void setBookmarks(List<Bookmark> bookmarks)
    {
        this.bookmarks = bookmarks;
        this.bookmarks.sort(Comparator.comparing(Bookmark::getSecondsFromStart));
    }

    public void addBookmark(Bookmark bookmark)
    {
        this.bookmarks.add(bookmark);
        this.bookmarks.sort(Comparator.comparing(Bookmark::getSecondsFromStart));
    }
}
