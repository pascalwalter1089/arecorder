package com.pascalwalter.arecorder.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.pascalwalter.arecorder.R;

public class StartNewSessionDialogFragment extends DialogFragment
{
    NoticeDialogListener listener;

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View inflatedVIew =
                requireActivity().getLayoutInflater().inflate(R.layout.new_session_dialog,
                                                                   null);
        builder.setView(inflatedVIew).setPositiveButton(
                "Starten",
                (dialogInterface, i) ->
                {
                    String sessionName =
                            ((EditText) inflatedVIew.findViewById(R.id.sessionNameEditText)).getText().toString();
                    listener.onDialogPositiveClick(StartNewSessionDialogFragment.this, sessionName);
                }
        )
                .setNegativeButton("Abbrechen", ((dialogInterface, i) -> {
                    listener.onDialogNegativeClick(StartNewSessionDialogFragment.this);
                })).setTitle("Neue Session starten.");

        return builder.create();
    }

    public interface NoticeDialogListener {
        void onDialogPositiveClick(DialogFragment dialog, String sessionName);
        void onDialogNegativeClick(DialogFragment dialog);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (NoticeDialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface. Throw exception.
            throw new ClassCastException(listener.toString()
                                                 + " must implement NoticeDialogListener");
        }

    }
}
