package com.pascalwalter.arecorder.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pascalwalter.arecorder.R;
import com.pascalwalter.arecorder.models.Bookmark;
import com.pascalwalter.arecorder.util.TimestampToStringConverter;

public class BookmarkRecyclerViewAdapter extends RecyclerView.Adapter<BookmarkRecyclerViewAdapter.ViewHolder>
{
    private Bookmark[] mSessionBookmarks;

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        private final TextView mBookmarkLabel;
        private final TextView mTimestampLabel;
        public ViewHolder(View view) {
            super(view);
            mBookmarkLabel = view.findViewById(R.id.bookmarkLabel);
            mTimestampLabel = view.findViewById(R.id.timestampLabel);
        }

        public void setBookmarkLabel(String label)
        {
            this.mBookmarkLabel.setText(label);
        }

        public void setTimestampLabel(int timestamp)
        {
            this.mTimestampLabel.setText(TimestampToStringConverter.getFormattedTimestamp(timestamp));
        }
    }

    public BookmarkRecyclerViewAdapter(Bookmark[] dataSet) {
        mSessionBookmarks = dataSet;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bookmark_row_item, parent, false);

        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        holder.setBookmarkLabel(mSessionBookmarks[position].getName());
        holder.setTimestampLabel(mSessionBookmarks[position].getSecondsFromStart());
    }

    @Override
    public int getItemCount()
    {
        return mSessionBookmarks.length;
    }

    public void updateData(Bookmark[] newDataSet)
    {
        this.mSessionBookmarks = newDataSet;
        this.notifyDataSetChanged();
    }

}
